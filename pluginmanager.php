<?php

class BootstrapPluginManager {

	private static $plugins = array();

	public static function GetPlugin($type) {
		if (isset(BootstrapPluginManager::$plugins[$type])) {
			return BootstrapPluginManager::$plugins[$type];
		}
		return null;
	}

	public static function RegisterPlugin(IBootstrapPlugin $plugin) {
		$type = $plugin->getTypeInfo();
		BootstrapPluginManager::$plugins[$type] = $plugin;
	}
}

?>