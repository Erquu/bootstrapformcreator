(function(global) {
	
	var FormValidator = function(formName) {
		this.form = null;
		this.formName = formName;
		this.loaded = false;

		this.validators = {};

		global.addEventListener('load', this);

		console.log("FormValidator initialized");
	};

	FormValidator.prototype = {
		parseForm: function() {
			this.form = document.forms[this.formName];
			var elementCount = this.form.length;

			for (var i = 0; i < elementCount; i++) {
				var element = this.form[i];
				var dependencyTarget = element.getAttribute('tpl-target');
				if (!!dependencyTarget && !!this.form[dependencyTarget]) {
					var targetElement = this.form[dependencyTarget];
					var dependencyEval = element.getAttribute('tpl-eval');

					if (!this.validators[dependencyTarget]) {
						this.validators[dependencyTarget] = {
							validate: {}
						};
						console.log(targetElement.nodeName);
						if (targetElement.nodeName === 'INPUT') {
							targetElement.addEventListener('keyup', this);
						} else {
							targetElement.addEventListener('change', this);
						}
					}
					this.validators[dependencyTarget].validate[element.name] = dependencyEval;

					element.disabled = (element.value !== dependencyEval);
				}
			}
		},
		disable: function(element, disable) {
			if (element instanceof NodeList) {
				for (var i = 0; i < element.length; i++) {
					this.disable(element[i], disable);
				}
			} else {
				if (disable) {
					element.disabled = true;
				} else {
					element.removeAttribute('disabled');
				}
			}
		},
		handleEvent: function(ev) {
			switch (ev.type) {
				case 'load':
					this.parseForm();
					break;
				case 'change':
				case 'keyup':
					var element = ev.srcElement || ev.target;
					if (!!this.validators[element.name]) {
						var validators = this.validators[element.name].validate;
						for (var key in validators) {
							this.disable(this.form[key], (element.value !== validators[key]))
						}
					}
					break;
			}
		}
	};

	global.FormValidator = FormValidator;

})(this);