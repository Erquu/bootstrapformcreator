<?php

interface IBootstrapPlugin {
	function getTypeInfo();
	function call($data);
}

?>