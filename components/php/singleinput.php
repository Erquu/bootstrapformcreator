<?php

use Tempel\Component;

class SingleInput extends Component {
	
	public function __construct($type, $label, $options=array()) {

		if (!is_array($options)) {
			$options = array();
		}

		$this->setValue('type', $type);
		$this->setValue('label', $label);
		$this->setValue('attributes', $options);
	}
	
}

?>