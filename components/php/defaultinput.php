<?php

use Tempel\Component;

class DefaultInput extends Component {

	public function __construct($input, $label, $helptext=NULL) {
		$this->setValue('label', $label);
		$this->setValue('input', $input);

		if ($helptext !== NULL) {
			$this->setValue('helptext', new HelpBlock($helptext));
		}
	}
}

?>