<?php

use Tempel\IComponent;
use Tempel\TemplatelessComponent;

class FormElement extends TemplatelessComponent {

	private $content = NULL;

	/**
	 * 
	 */
	public function __construct($formObject=NULL) {
		$type = $formObject['type'];

		$plugin = BootstrapPluginManager::getPlugin($type);
		if (!is_null($plugin)) {
			$this->content = $plugin->call($formObject);
		} else {
			$helptext = isset($formObject['helptext']) ? $formObject['helptext'] : NULL;
			$attributes = isset($formObject['attributes']) ? $formObject['attributes'] : array();

			$name = isset($formObject['name']) ? $formObject['name'] : NULL;
			$label = isset($formObject['label']) ? $formObject['label'] : NULL;

			if (!is_array($attributes)) {
				$attributes = array();
			}
			if (isset($name)) {
				$attributes['name'] = $name;
			}
			if (isset($formObject['dependency'])) {
				$attributes['tpl-target'] = $formObject['dependency']['name'];
				$attributes['tpl-eval'] = $formObject['dependency']['value'];
			}

			switch ($type) {
				case 'file':
					$input = new Input($type, $attributes);
					$element = new DefaultInput($input, $label, $helptext);
					$formgroup = new FormGroup($element);
					$this->content = $formgroup;
					break;
				case 'hidden':
				case 'image':
				case 'text':
				case 'password':
				case 'color':
				case 'date':
				case 'datetime':
				case 'datetime-local':
				case 'email':
				case 'month':
				case 'number':
				case 'range':
				case 'search':
				case 'tel':
				case 'time':
				case 'url':
				case 'week':
					if (is_array($attributes)) {
						if (!isset($attributes['class'])) {
							$attributes['class'] = 'form-control';
						}
					}

					$input = new Input($type, $attributes);
					$element = new DefaultInput($input, $label, $helptext);
					$formgroup = new FormGroup($element);
					$this->content = $formgroup;
					break;
				// Other Types
				case 'textarea':
					if (is_array($attributes)) {
						if (!isset($attributes['class'])) {
							$attributes['class'] = 'form-control';
						}
					}
					$input = new Textarea($attributes);
					$element = new DefaultInput($input, $label, $helptext);
					$formgroup = new FormGroup($element);
					$this->content = $formgroup;
					break;
				case 'select':
				//case 'option':
					if (is_array($attributes)) {
						if (!isset($attributes['class'])) {
							$attributes['class'] = 'form-control';
						}
					}
					$options = isset($formObject['options']) ? $formObject['options'] : array();

					$input = new Select($options, $attributes);
					$element = new DefaultInput($input, $label, $helptext);
					$formgroup = new FormGroup($element);
					$this->content = $formgroup;
					break;
				case 'button':
				case 'reset':
				case 'submit':
					$buttonType = isset($formObject['buttonType']) ? $formObject['buttonType'] : BT_BUTTON_DEFAULT;

					$element = new ButtonInput($type, $label, $buttonType, $attributes);
					$offset = new OffsetDiv($element);
					$formgroup = new FormGroup($offset);
					$this->content = $formgroup;
					break;
				case 'radio':
				case 'checkbox':
					$element = new SingleInput($type, $label, $attributes);
					$offset = new OffsetDiv($element);
					$formgroup = new FormGroup($offset);
					$this->content = $formgroup;
					//$this->content = $offset;
					break;
			}
		}
	}

	public function parse() {
		if ($this->content instanceof IComponent) {
			return $this->content->parse();
		} else {
			return $this->content;
		}
	}

}

?>