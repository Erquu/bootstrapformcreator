<?php

use Tempel\Component;

const BT_BUTTON_DEFAULT = 'default';
const BT_BUTTON_PRIMARY = 'primary';
const BT_BUTTON_SUCCESS = 'success';
const BT_BUTTON_INFO = 'info';
const BT_BUTTON_WARNING = 'warning';
const BT_BUTTON_DANGER = 'danger';
const BT_BUTTON_LINK = 'link';


class ButtonInput extends Component {

	public function __construct($type, $label, $buttontype, $options) {
		//$this->setValue('for', $id);
		$this->setValue('label', $label);
		$this->setValue('type', $type);
		$this->setValue('attributes', $options);

		switch ($buttontype) {
			case BT_BUTTON_DEFAULT:
			case BT_BUTTON_PRIMARY:
			case BT_BUTTON_SUCCESS:
			case BT_BUTTON_INFO:
			case BT_BUTTON_WARNING:
			case BT_BUTTON_DANGER:
			case BT_BUTTON_LINK:
				// Do nothing (just to make sure a valid button type is given)
				break;
			default:
				$buttontype = BT_BUTTON_DEFAULT;
				break;
		}

		$this->setValue('btnclass', "btn-$buttontype");
	}
}

?>