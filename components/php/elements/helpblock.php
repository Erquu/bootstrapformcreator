<?php

use Tempel\Component;

class HelpBlock extends Component {

	public function __construct($content) {
		$this->setValue('content', $content);
	}
}

?>