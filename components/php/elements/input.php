<?php

use Tempel\Component;

class Input extends Component {

	public function __construct($type, $attr=NULL) {
		$this->setValue('type', $type);
		if (is_array($attr)) {
			$this->setValue('attributes', $attr);
		}
	}
}

?>