<?php

use Tempel\Component;

class Option extends Component {

	public function __construct($value, $label) {
		$this->setValue('value', $value);
		$this->setValue('label', $label);
	}
}

?>