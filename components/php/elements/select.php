<?php

//use Tempel\Component;

class Select extends TemplateList {

	public function __construct($options, $attributes) {
		parent::__construct('options');
		
		foreach ($options as $key => $value) {
			$this->addComponent(new Option($key, $value));
		}

		if (is_array($attributes)) {
			$this->setValue('attributes', $attributes);
		}
	}
}

?>