<?php

class FormHorizontal extends TemplateList {

	public function __construct($name=NULL, $method='POST', $action=NULL) {
		parent::__construct('content');

		$attributes = array(
			'method' => $method
		);
		if (isset($action)) {
			$attributes['action'] = $action;
		}
		if (isset($name)) {
			$attributes['name'] = $name;
		}

		$this->setValue('attributes', $attributes);
	}
}

?>