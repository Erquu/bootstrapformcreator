<?php

class BootstrapFormBuilder {

	private $path = NULL;
	private $method = 'POST';
	private $action = '';
	private $formName = '';
	private $formObject = NULL;

	private $callbackInstance = NULL;
	private $callbackFunction = NULL;
	private $callback = array();

	public function __construct($path) {
		$this->path = $path;
	}

	private function createEmptyForm() {
		$basename = basename($this->path);
		$name = substr($basename, 0, strpos($basename, '.'));
		$form = array(
			'name' => $name,
			'method' => 'POST',
			'action' => '',
			'verify' => true,
			'fields' => array()
		);
		$content = json_encode($form, 128);
		$file = fopen($this->path, "w");
		fwrite($file, $content);
		fclose($file);
	}

	private function invokeListener($name, $args) {
		if (isset($this->callback[$name])) {
			return call_user_func_array($this->callback[$name], $args);
		}
		return NULL;
	}

	public function addEventListener($type, $function, $instance=NULL) {
		$callback = NULL;
		if ($instance !== NULL) {
			$callback = array($instance, $function);
		} else {
			$callback = $function;
		}

		$this->callback[$type] = $callback;
	}

	public function parse() {
		if (!file_exists($this->path)) {
			$this->createEmptyForm();
		}
		$json = file_get_contents($this->path);
		$json = json_decode($json, true);

		$method = 'POST';
		$action = '';
		$this->formName = $json['name'];

		$verify = (isset($json['verify']) && $json['verify']);

		if (isset($json['method'])) {
			$method = $json['method'];
		}
		if (isset($json['action'])) {
			$action = $json['action'];
		}

		$form = new FormHorizontal($this->formName, $method, $action);

		$this->invokeListener('buildStart', array($form, $json));

		$num = 0;
		foreach ($json['fields'] as $value) {
			$component = new FormElement($value);
			$componentTmp = $this->invokeListener('iterate', array($component, $value, $num));
			if ($componentTmp !== NULL) {
				$component = $componentTmp;
			}
			$form->addComponent($component);
			$num++;
		}

		$this->invokeListener('buildEnd', array($form, $json));

		$this->formObject = $form;
	}

	public function getForm() {
		return $this->formObject;
	}

	public function getHead() {
		return '<script type="text/javascript">new FormValidator(\'' . $this->formName . '\')</script>';
	}

	public function getScript() {
		$script = '<script type="text/javascript">';
		$script .= file_get_contents(__DIR__ . '/js/formvalidator.js');
		$script .= '</script>';

		return $script;
	}
}